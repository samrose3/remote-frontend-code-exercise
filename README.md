# Remote Employee Manager

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and has some slightly modified ESLint and Babel configurations using [react-app-rewired](https://github.com/timarney/react-app-rewired).

## Overview

This is a simple employee management app with two pages:

- **Employees list** - Display a list of people and their attributes
  - **Employee attributes**
    - Name (text)
    - Date of birth (text or date)
    - Job title (text)
    - Country (select)
    - Salary (text or number)
- **Add / Edit employee** - A page to create or edit an existing employee details

| Home                    | Add                   | Edit                    |
|-------------------------|-----------------------|-------------------------|
| ![Home](/docs/home.png) | ![Add](/docs/add.png) | ![Edit](/docs/edit.png) |

## Getting Started

If you haven't already done so, [install yarn](https://yarnpkg.com/en/docs/install) and run these commands:

```sh
git clone git@gitlab.com:samrose3/remote-frontend-code-exercise.git
cd remote-frontend-code-exercise
yarn # install dependencies
```

Once installed, run in the app in development mode with `yarn start` then visit [http://localhost:3000](http://localhost:3000).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn format`

Use [Prettier](https://prettier.io/) to apply an opinionated formatting to all files in the `src` directory.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

So please don't eject this project 😅.
