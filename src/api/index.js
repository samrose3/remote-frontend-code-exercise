import { employees, countries } from './api.mock'

const COUNTRIES_KEY = 'remote__countries_list'
const EMPLOYEES_KEY = 'remote__employees_list'

export function getCountries() {
  let data = localStorage.getItem(COUNTRIES_KEY)

  if (data) return JSON.parse(data)
  return new Promise((resolve) =>
    setTimeout(() => {
      resolve(countries)
      localStorage.setItem(COUNTRIES_KEY, JSON.stringify(countries))
    }, 1000),
  )
}

export function getEmployees() {
  // let data = localStorage.getItem(EMPLOYEES_KEY)

  // if (data) return JSON.parse(data)
  return new Promise((resolve) =>
    setTimeout(() => {
      resolve(employees)
      localStorage.setItem(EMPLOYEES_KEY, JSON.stringify(employees))
    }, 2000),
  )
}
