import styled from '@emotion/styled'

const diameter = 36

const Avatar = styled.div`
  border-radius: var(--radius-full);
  background-color: var(--theme-color-secondary-light);
  width: ${({ size = diameter }) => `${size}px`};
  height: ${({ size = diameter }) => `${size}px`};
`

export default Avatar
