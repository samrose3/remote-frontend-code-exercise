import { css } from '@emotion/react'
import styled from '@emotion/styled'

import { createPropStyles } from 'utils'

const variants = createPropStyles('variant', {
  primary: css`
    border-color: var(--theme-color-primary);
    background-color: var(--theme-color-primary);
    color: var(--theme-background-color-alt);
    &:hover {
      filter: brightness(0.85);
      box-shadow: var(--shadow-primary);
    }
  `,
})

const Button = styled.button`
  display: inline-block;
  cursor: pointer;
  min-width: 143px;
  padding: var(--space-2) var(--space-3);
  border-radius: var(--radius-full);
  border: 2px solid var(--theme-color-primary-light);
  background-color: transparent;
  color: var(--theme-color-primary);
  font-size: var(--font-size-base);
  font-weight: 500;
  text-align: center;
  text-decoration: none;
  transition-duration: 0.3s;
  transition-property: border-color, background, color, box-shadow, filter;

  &:hover {
    border-color: var(--theme-color-primary);
    background-color: var(--theme-color-primary);
    color: var(--theme-background-color-alt);
  }

  &:focus {
    outline: var(--theme-color-primary-light) solid 2px;
  }

  ${variants}
`

export default Button
