import styled from '@emotion/styled'

const Section = styled.div`
  padding: var(--space-4);
  border-bottom: 1px solid var(--theme-border-color);
`

const Card = styled.div`
  background-color: var(--theme-background-color-alt);
  border-radius: var(--radius);
  box-shadow: var(--shadow);

  ${Section}:first-of-type {
    border-top-left-radius: var(--radius);
    border-top-right-radius: var(--radius);
  }

  ${Section}:last-of-type {
    border-bottom-left-radius: var(--radius);
    border-bottom-right-radius: var(--radius);
    border-bottom: none;
  }
`

Card.Section = Section

export default Card
