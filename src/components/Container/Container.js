import styled from '@emotion/styled'
import { BREAKPOINTS, EXTRA_WIDE_WIDTH } from '../../constants'

const Container = styled.div`
  width: 100%;
  max-width: ${({ maxWidth = EXTRA_WIDE_WIDTH }) => `${maxWidth}px`};
  margin-left: auto;
  margin-right: auto;
  padding-left: 2rem;
  padding-right: 2rem;
  @media ${BREAKPOINTS.sm} {
    padding-left: 1rem;
    padding-right: 1rem;
  }
`

export default Container
