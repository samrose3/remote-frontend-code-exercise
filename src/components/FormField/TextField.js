import styled from '@emotion/styled'

const FormField = styled.div`
  display: grid;
  grid-gap: var(--space-2);
  width: 100%;
`

export default FormField
