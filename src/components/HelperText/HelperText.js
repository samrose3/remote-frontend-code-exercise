import React from 'react'
import Text from 'components/Text'

const HelperText = (props) => <Text as="p" size="sm" variant="alt" {...props} />

export default HelperText
