import React from 'react'

import SVG from 'components/SVG'

const Icon = ({ size = '1em', ...props }) => (
  <SVG width={size} height={size} viewBox="0 0 16 16" {...props} />
)

export default Icon
