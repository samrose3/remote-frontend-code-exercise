import React from 'react'
import styled from '@emotion/styled'

import InputBase from 'components/InputBase'

const Input = styled.input`
  position: relative;
  width: 100%;
  outline: none;
  margin: 0;
  padding: var(--space-2) 0;
  border: 0;
  background: none;
  font-size: var(--font-size-md);

  &::placeholder {
    color: var(--theme-color-alt);
  }

  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  input[type='number'] {
    -moz-appearance: textfield;
  }
`

export default ({
  autoComplete,
  defaultValue,
  disabled,
  id,
  inputProps,
  helperText,
  helperTextProps,
  label,
  labelProps,
  name,
  onChange,
  placeholder,
  readOnly,
  required,
  type,
  value,
  ...props
}) => {
  const inputPropsFinal = {
    autoComplete,
    defaultValue,
    disabled,
    id,
    name,
    onChange,
    placeholder,
    readOnly,
    required,
    type,
    value,
    ...inputProps,
  }

  return (
    <InputBase {...props}>
      <Input {...inputPropsFinal} />
    </InputBase>
  )
}
