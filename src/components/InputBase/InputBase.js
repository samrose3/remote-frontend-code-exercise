import styled from '@emotion/styled'

const InputBase = styled.div`
  position: relative;
  width: 100%;
  outline: none;
  border: 0;
  font-size: var(--font-size-md);
  font-weight: 400;

  &::before,
  &::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 2px;
    transition: transform 200ms cubic-bezier(0, 0, 0.2, 1) 0ms;
    transform-origin: center bottom;
  }

  &::before {
    background-color: var(--theme-border-color);
    transform: scaleY(0.5);
  }

  &:hover::before {
    transform: scaleY(1);
  }

  &::after {
    background-color: var(--theme-color-primary);
    transform: scaleX(0);
  }

  &:focus-within::after {
    transform: scaleX(1);
  }
`

export default InputBase
