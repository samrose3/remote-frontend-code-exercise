import React from 'react'
import Text from 'components/Text'

const Label = (props) => (
  <Text as="label" htmlFor={props.id} size="sm" variant="muted" {...props} />
)

export default Label
