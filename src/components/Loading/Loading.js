import React from 'react'
import styled from '@emotion/styled'
import { keyframes } from '@emotion/react'

const Loading = () => (
  <CubeGrid>
    <Cube />
    <Cube />
    <Cube />
    <Cube />
    <Cube />
    <Cube />
    <Cube />
    <Cube />
    <Cube />
  </CubeGrid>
)

const animation = keyframes`
  0%,
  70%,
  100% {
    transform: scale3D(1, 1, 1);
  }
  35% {
    transform: scale3D(0, 0, 1);
  }
}
`

const CubeGrid = styled.div`
  width: 40px;
  height: 40px;
  margin: 25vh auto;
`

const Cube = styled.div`
  width: 33%;
  height: 33%;
  background-color: var(--theme-color-primary);
  float: left;
  animation: ${animation} 2s infinite ease-in-out;

  &:nth-of-type(1) {
    animation-delay: 0.2s;
  }
  &:nth-of-type(2) {
    animation-delay: 0.3s;
  }
  &:nth-of-type(3) {
    animation-delay: 0.4s;
  }
  &:nth-of-type(4) {
    animation-delay: 0.1s;
  }
  &:nth-of-type(5) {
    animation-delay: 0.2s;
  }
  &:nth-of-type(6) {
    animation-delay: 0.3s;
  }
  &:nth-of-type(7) {
    animation-delay: 0s;
  }
  &:nth-of-type(8) {
    animation-delay: 0.1s;
  }
  &:nth-of-type(9) {
    animation-delay: 0.2s;
  }
`

export default Loading
