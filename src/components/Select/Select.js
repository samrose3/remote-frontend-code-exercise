/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useCallback } from 'react'
import styled from '@emotion/styled'

import InputBase from 'components/InputBase'
import ArrowDownIcon from 'icons/ArrowDownIcon'

const Select = styled.select`
  position: relative;
  width: 100%;
  outline: none;
  margin: 0;
  padding: var(--space-2) 0;
  border: 0;
  background: none;
  font-size: var(--font-size-md);
  appearance: none;

  color: ${(props) =>
    !props['data-selected'] ? 'var(--theme-color-alt)' : 'inherit'};
`

export default ({
  autoComplete,
  children,
  defaultValue,
  disabled,
  id,
  selectProps,
  helperText,
  helperTextProps,
  label,
  labelProps,
  name,
  onChange,
  placeholder,
  readOnly,
  required,
  type,
  value,
  ...props
}) => {
  const [selected, setSelected] = useState(defaultValue || value)
  const selectPropsFinal = {
    autoComplete,
    children,
    defaultValue,
    disabled,
    id,
    name,
    onChange,
    placeholder,
    readOnly,
    required,
    type,
    value,
    ...selectProps,
  }

  const handleOnChange = useCallback(
    (e) => {
      setSelected(e.target.value)
      if (onChange) onChange(e)
    },
    [onChange],
  )

  return (
    <InputBase {...props}>
      <ArrowDownIcon
        css={{
          position: 'absolute',
          top: '50%',
          right: 'var(--space-2)',
          transform: 'translateY(-50%)',
          color: 'inherit',
        }}
      />
      <Select
        {...selectPropsFinal}
        data-selected={selected}
        onChange={handleOnChange}
      />
    </InputBase>
  )
}
