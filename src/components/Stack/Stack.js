import React from 'react'
import styled from '@emotion/styled'

export const StackItem = styled.div`
  display: flex;
  flex: 0 0 auto;
  min-width: 0;
`

const alignments = {
  start: { alignItems: 'flex-start' },
  end: { alignItems: 'flex-end' },
  center: { alignItems: 'center' },
  fill: {
    alignItems: 'stretch',
    [`& > ${StackItem} > *`]: {
      height: '100%',
    },
  },
  baseline: { alignItems: 'baseline' },
}

const distributions = {
  start: { justifyContent: 'flex-start' },
  end: { justifyContent: 'flex-end' },
  center: { justifyContent: 'center' },
  around: { justifyContent: 'space-around' },
  between: { justifyContent: 'space-between' },
  evenly: { justifyContent: 'space-evenly' },
  fill: {
    [`& > ${StackItem}`]: {
      flex: '1 1 auto',
    },
  },
  fillEvenly: {
    [`& > ${StackItem}`]: {
      flex: '1 0',
      minWidth: 'fit-content',
    },
  },
}

const StyledStack = styled.div(
  ({
    alignment = 'start',
    distribution = 'start',
    spacing = 'var(--space-3)',
    vertical = false,
    wrap = true,
  }) => {
    const negativeSpacing = `calc(-1 * ${spacing})`

    return [
      {
        display: 'flex',
        alignItems: 'stretch',
        marginTop: negativeSpacing,
        marginLeft: negativeSpacing,
        flexWrap: wrap ? 'wrap' : 'nowrap',
        [StackItem]: {
          marginTop: spacing,
          marginLeft: spacing,
          maxWidth: '100%',
        },
      },
      alignments[alignment],
      distributions[distribution],
      vertical && {
        flexDirection: 'column',
        marginLeft: 0,
        [`& > ${StackItem}`]: {
          marginLeft: 0,
        },
      },
    ]
  },
)

const Stack = ({ children, ...props }) => {
  const items = React.Children.map(children, (child) => (
    <StackItem>{child}</StackItem>
  ))

  return <StyledStack {...props}>{items}</StyledStack>
}

export default Stack
