import styled from '@emotion/styled'

const Table = styled.table`
  width: 100%;
  border-spacing: 0 1rem;

  th {
    padding: 0 var(--space-4);
    color: var(--theme-color-gray);
    font-size: var(--font-size-sm);
    font-weight: 600;
    text-align: left;
    text-transform: uppercase;
    vertical-align: bottom;
  }

  td {
    vertical-align: middle;
  }

  tbody tr {
    border-radius: var(--radius);
    box-shadow: var(--shadow);
    margin-bottom: var(--space-4);
  }

  tbody td {
    padding: var(--space-4);
    background-color: var(--theme-background-color-alt);

    &:first-of-type {
      border-top-left-radius: var(--radius);
      border-bottom-left-radius: var(--radius);
    }

    &:last-of-type {
      border-top-right-radius: var(--radius);
      border-bottom-right-radius: var(--radius);
    }
  }
`

export default Table
