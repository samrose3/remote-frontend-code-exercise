import styled from '@emotion/styled'

const variants = {
  alt: { color: 'var(--theme-color-alt)' },
  muted: { color: 'var(--theme-color-gray)' },
}

const Text = styled.span(
  ({ size, weight }) => ({
    fontSize: size && `var(--font-size-${size})`,
    fontWeight: weight,
  }),
  ({ variant }) => variant && variants[variant],
)

export default Text
