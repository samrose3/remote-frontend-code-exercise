import React, { useState } from 'react'

const AuthContext = React.createContext()

const defaultUser = {
  fullName: 'Julie Howard',
  role: 'Admin',
}

function AuthProvider({ children }) {
  const [user, setUser] = useState(defaultUser)

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      {children}
    </AuthContext.Provider>
  )
}

export function useAuth() {
  const context = React.useContext(AuthContext)

  if (context === undefined) {
    throw new Error('useAuth must be used within a AuthProvider')
  }

  return context
}

export default AuthProvider
