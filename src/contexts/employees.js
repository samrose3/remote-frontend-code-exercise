import React, { useContext, useEffect, useReducer } from 'react'

import useFetchEmployees from 'hooks/useFetchEmployees'

const EmployeesContext = React.createContext()

export const ACTIONS = {
  ADD: 'add',
  EDIT: 'edit',
  SET: 'set',
}

export function employeesReducer(state, action) {
  switch (action.type) {
    case ACTIONS.ADD: {
      const newState = [
        ...state,
        {
          ...action.payload,
          id: state.length,
        },
      ]
      return newState
    }
    case ACTIONS.EDIT: {
      const index = state.findIndex((e) => e.id === action.payload.id)
      const newState = [...state]
      newState[index] = action.payload
      return newState
    }
    case ACTIONS.SET: {
      const newState = [...action.payload]
      return newState
    }
    default: {
      return state
    }
  }
}

function EmployeesProvider({ children }) {
  const [state, dispatch] = useReducer(employeesReducer, [])
  const data = useFetchEmployees()

  useEffect(
    () =>
      dispatch({
        type: ACTIONS.SET,
        payload: data,
      }),
    [data, dispatch],
  )

  return (
    <EmployeesContext.Provider value={[state, dispatch]}>
      {children}
    </EmployeesContext.Provider>
  )
}

export function useEmployees() {
  const context = useContext(EmployeesContext)

  if (context === undefined) {
    throw new Error('useEmployees must be used within a EmployeesProvider')
  }

  return context
}

export default EmployeesProvider
