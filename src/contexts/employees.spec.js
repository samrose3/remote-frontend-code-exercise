import { employees as employeesMock } from 'api/api.mock'
import { employeesReducer, ACTIONS } from './employees'

const JaneDoe = {
  name: 'Jane Doe',
  dateOfBirth: '04/12/1990',
  jobTitle: 'Test automation engineer',
  country: 'United States',
  salary: 60000,
}

// edit over the first employee in the employeesMock array
const EditedEmployee = {
  ...employeesMock[0],
  jobTitle: 'Author',
  country: 'Portugal',
  salary: 60000,
}

describe('employees reducer', () => {
  it('should handle initial state', () => {
    expect(employeesReducer([], {})).toEqual([])
  })

  it('should handle ADD_EMPLOYEE', () => {
    expect(
      employeesReducer([], {
        type: ACTIONS.ADD,
        payload: JaneDoe,
      }),
    ).toEqual([{ ...JaneDoe, id: 0 }])

    expect(employeesReducer(employeesMock, {})).toEqual(employeesMock)

    expect(
      employeesReducer(employeesMock, {
        type: ACTIONS.ADD,
        payload: JaneDoe,
      }),
    ).toEqual([...employeesMock, { ...JaneDoe, id: employeesMock.length }])
  })

  it('should handle EDIT_EMPLOYEE', () => {
    expect(
      employeesReducer(employeesMock, {
        type: ACTIONS.EDIT,
        payload: EditedEmployee,
      }),
    ).toEqual([EditedEmployee, ...employeesMock.slice(1)])
  })

  it('should handle SET_EMPLOYEE', () => {
    expect(
      employeesReducer(employeesMock, {
        type: ACTIONS.SET,
        payload: employeesMock,
      }),
    ).toEqual(employeesMock)
  })
})
