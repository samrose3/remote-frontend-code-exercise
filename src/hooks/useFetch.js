import { useState, useEffect } from 'react'

const useFetch = (asyncFn, initialState) => {
  const [state, setState] = useState(initialState)

  useEffect(() => {
    async function fetchData() {
      const data = await asyncFn()
      setState(data)
    }

    fetchData()
  }, [asyncFn])

  return state
}

export default useFetch
