import { getCountries } from 'api'
import useFetch from './useFetch'

const useFetchCountries = () => {
  return useFetch(getCountries, [])
}

export default useFetchCountries
