import { getEmployees } from 'api'
import useFetch from './useFetch'

const useFetchEmployees = () => {
  return useFetch(getEmployees, [])
}

export default useFetchEmployees
