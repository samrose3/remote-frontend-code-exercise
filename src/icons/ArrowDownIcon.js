import React from 'react'
import { createIconComponent } from './'

const ArrowDownIcon = createIconComponent(
  <path d="M13.764 6.335L9.05 11.05a.759.759 0 01-1.1 0L3.234 6.335a.76.76 0 010-1.1.76.76 0 011.1 0L8.5 9.4l4.165-4.163a.76.76 0 011.1 0 .759.759 0 010 1.1z" />,
)

export default ArrowDownIcon
