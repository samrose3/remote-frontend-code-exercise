import React from 'react'
import Icon from 'components/Icon'

export const createIconComponent = (path) => (props) => (
  <Icon {...props}>{path}</Icon>
)
