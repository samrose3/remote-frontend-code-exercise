import React from 'react'
import { render } from 'react-dom'
import { Router } from '@reach/router'

import AuthProvider from 'contexts/auth'
import IndexPage from 'pages'
import AddPage from 'pages/add'
import EditPage from 'pages/edit'
import EmployeesProvider from 'contexts/employees'

render(
  <AuthProvider>
    <EmployeesProvider>
      <Router>
        <IndexPage path="/" />
        <AddPage path="add" />
        <EditPage path="edit/:employeeId" />
      </Router>
    </EmployeesProvider>
  </AuthProvider>,
  document.getElementById('root'),
)
