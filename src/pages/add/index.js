import React from 'react'
import styled from '@emotion/styled'

import App from 'views/layout/Layout'
import EmployeeAdd from 'views/employee/EmployeeAdd'

const AddPage = () => (
  <App>
    <Wrapper>
      <EmployeeAdd />
    </Wrapper>
  </App>
)

const Wrapper = styled.div`
  display: grid;
  margin-top: var(--space-5);
`

export default AddPage
