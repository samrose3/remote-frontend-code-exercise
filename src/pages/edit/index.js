import React from 'react'
import styled from '@emotion/styled'

import Layout from 'views/layout/Layout'
import EmployeeEdit from 'views/employee/EmployeeEdit'

const EditPage = (props) => (
  <Layout>
    <Wrapper>
      <EmployeeEdit employeeId={props.employeeId} />
    </Wrapper>
  </Layout>
)

const Wrapper = styled.div`
  display: grid;
  margin-top: var(--space-5);
`

export default EditPage
