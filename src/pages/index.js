import React from 'react'
import styled from '@emotion/styled'
import { Link } from '@reach/router'

import Layout from 'views/layout/Layout'
import Button from 'components/Button'
import Stack from 'components/Stack'
import Text from 'components/Text'
import PersonalIcon from 'icons/PersonalIcon'

import EmployeeCount from 'views/employees/EmployeeCount'
import EmployeeList from 'views/employees/EmployeeList'

const IndexPage = () => {
  return (
    <Layout>
      <Wrapper>
        <Stack alignment="center" distribution="between">
          <Stack alignment="baseline" spacing="var(--space-2)">
            <Text as="h1" size="xl" weight="500">
              People
            </Text>
            <EmployeeCount />
          </Stack>
          <Button as={Link} to="add" variant="primary">
            <Stack alignment="center" spacing="var(--space-2)">
              <PersonalIcon />
              <span>Add employee</span>
            </Stack>
          </Button>
        </Stack>
        <EmployeeList />
      </Wrapper>
    </Layout>
  )
}

const Wrapper = styled.div`
  display: grid;
  margin-top: var(--space-4);
  grid-gap: var(--space-5);
`

export default IndexPage
