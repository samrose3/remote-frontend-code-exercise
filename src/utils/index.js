/**
 *
 * @param {string} propName Name of the prop to control the styles
 * @param {object} styles CSS object containing prop-based styles
 */
export const createPropStyles = (propName, styles) => (props) =>
  props[propName] ? styles[props[propName]] : ''
