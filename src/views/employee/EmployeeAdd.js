import React from 'react'

import { ACTIONS } from 'contexts/employees'
import EmployeeForm from './EmployeeForm'

const EmployeeAdd = () => {
  return (
    <EmployeeForm
      title="Add a new employee"
      subtitle="Fill out the information of your new employee"
      confirmationText="Add employee"
      actionType={ACTIONS.ADD}
    />
  )
}

export default EmployeeAdd
