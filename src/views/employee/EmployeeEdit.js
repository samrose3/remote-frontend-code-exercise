import React from 'react'

import { ACTIONS } from 'contexts/employees'
import EmployeeForm from './EmployeeForm'

const EmployeeEdit = ({ employeeId }) => {
  return (
    <EmployeeForm
      title="Edit employee"
      subtitle="Edit the information of your employee."
      confirmationText="Save"
      employeeId={employeeId}
      actionType={ACTIONS.EDIT}
    />
  )
}

export default EmployeeEdit
