/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useCallback, useMemo } from 'react'
import { Link, useNavigate } from '@reach/router'
import { Formik } from 'formik'

import Button from 'components/Button'
import Card from 'components/Card'
import Container from 'components/Container'
import FormField from 'components/FormField'
import HelperText from 'components/HelperText'
import Input from 'components/Input'
import Label from 'components/Label'
import Loading from 'components/Loading'
import Select from 'components/Select'
import Stack from 'components/Stack'
import Text from 'components/Text'

import { READING_WIDTH } from '../../constants'
import useFetchCountries from 'hooks/useFetchCountries'
import { useEmployees } from 'contexts/employees'

const initialEmployee = {
  name: '',
  dateOfBirth: '',
  jobTitle: '',
  country: '',
  salary: '',
}

const EmployeeFormContainer = ({
  employeeId,
  confirmationText,
  title,
  subtitle,
  actionType,
}) => {
  const navigate = useNavigate()
  const countries = useFetchCountries()
  const [employees, dispatch] = useEmployees()

  const employee = useMemo(
    () =>
      employeeId
        ? employees.find((e) => e.id === parseInt(employeeId, 10))
        : initialEmployee,
    [employeeId, employees],
  )

  const onSubmit = useCallback(
    (payload) => {
      dispatch({
        type: actionType,
        payload,
      })
      navigate('/')
    },
    [actionType, dispatch, navigate],
  )

  if (!employee) return <Loading />

  return (
    <Formik initialValues={employee} onSubmit={onSubmit}>
      {({ values, handleChange, handleSubmit }) => (
        <Card as="form" onSubmit={handleSubmit}>
          <Card.Section>
            <Text
              as="h1"
              size="xl"
              weight="500"
              css={{ marginBottom: 'var(--space-2)' }}>
              {title}
            </Text>
            <Text as="p" variant="alt">
              {subtitle}
            </Text>
          </Card.Section>
          <Card.Section css={{ borderBottom: 'none' }}>
            <Container maxWidth={READING_WIDTH}>
              <Stack alignment="fill" spacing="var(--space-4)" vertical>
                <InputField
                  type="text"
                  id="employee-name"
                  name="name"
                  label="Name"
                  helperText="First and Last"
                  placeholder="e.g. Jane Doe"
                  value={values.name}
                  onChange={handleChange}
                />
                <InputField
                  type="text"
                  id="employee-date-of-birth"
                  name="dateOfBirth"
                  label="Birthdate"
                  helperText="DD/MM/YYYY"
                  placeholder="e.g. 17/02/1990"
                  value={values.dateOfBirth}
                  onChange={handleChange}
                />
                <InputField
                  type="text"
                  id="employee-job-title"
                  name="jobTitle"
                  label="Job title"
                  helperText="What is their role?"
                  placeholder="e.g. Product manager"
                  value={values.jobTitle}
                  onChange={handleChange}
                />
                <SelectField
                  id="employee-country"
                  name="country"
                  label="Country"
                  helperText="Where are they based?"
                  value={values.country}
                  onChange={handleChange}>
                  <option value hidden>
                    Portugal
                  </option>
                  {countries.map((country) => (
                    <option key={country} value={country}>
                      {country}
                    </option>
                  ))}
                </SelectField>
                <InputField
                  type="number"
                  id="employee-salary"
                  name="salary"
                  label="Salary"
                  helperText="Gross yearly salary"
                  placeholder="e.g. 50000"
                  value={values.salary}
                  onChange={handleChange}
                />
              </Stack>
            </Container>
          </Card.Section>
          <Card.Section css={{ background: 'var(--theme-color-gray-light)' }}>
            <Stack distribution="center">
              <Button as={Link} to="/">
                Cancel
              </Button>
              <Button type="submit" variant="primary">
                {confirmationText}
              </Button>
            </Stack>
          </Card.Section>
        </Card>
      )}
    </Formik>
  )
}

const InputField = ({ label, helperText, ...props }) => (
  <FormField>
    <Label htmlFor={props.id}>{label}</Label>
    <Input {...props} />
    <HelperText aria-describedby={props.id}>{helperText}</HelperText>
  </FormField>
)

const SelectField = ({ label, helperText, children, ...props }) => (
  <FormField>
    <Label htmlFor={props.id}>{label}</Label>
    <Select {...props}>{children}</Select>
    <HelperText aria-describedby={props.id}>{helperText}</HelperText>
  </FormField>
)

export default EmployeeFormContainer
