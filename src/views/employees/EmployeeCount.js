import React from 'react'

import { useEmployees } from 'contexts/employees'
import Text from 'components/Text'

const EmployeeList = () => {
  const [employees] = useEmployees()
  const employeeCount = employees.length

  return employeeCount ? (
    <Text as="span" variant="muted" size="sm" weight="500">
      {employeeCount} employees
    </Text>
  ) : null
}

export default EmployeeList
