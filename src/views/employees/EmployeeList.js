/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from '@reach/router'
import format from 'number-format.js'

import { useEmployees } from 'contexts/employees'

import Button from 'components/Button'
import Loading from 'components/Loading'
import Table from 'components/Table'
import Text from 'components/Text'

const EmployeeList = () => {
  const [employees] = useEmployees()

  if (!employees.length) return <Loading />

  return (
    <Table>
      <thead>
        <tr>
          <th>Employee</th>
          <th>Job Title</th>
          <th>Country</th>
          <th>Salary</th>
        </tr>
      </thead>
      <tbody>
        {employees.map((employee) => (
          <tr key={`${employee.id}-${employee.name}`}>
            <td>
              <Text weight="600">{employee.name}</Text>
              <br />
              <Text variant="muted" size="sm">
                {employee.dateOfBirth}
              </Text>
            </td>
            <td>
              <Text variant="alt">{employee.jobTitle}</Text>
            </td>
            <td>
              <Text variant="alt">{employee.country}</Text>
            </td>
            <td>
              <Text variant="alt">
                {format('#,##0.#', employee.salary)} USD
              </Text>
              <Text
                variant="muted"
                size="sm"
                css={{ marginLeft: 'var(--space-1)' }}>
                per year
              </Text>
            </td>
            <td>
              <Button as={Link} to={`edit/${employee.id}`}>
                Edit
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  )
}

export default EmployeeList
