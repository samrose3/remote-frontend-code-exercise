import React from 'react'
import { Global, css } from '@emotion/react'

const GlobalStyles = () => (
  <Global
    styles={css`
      @import url('https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600&display=swap');

      :root {
        --font-family: Inter, -apple-system, BlinkMacSystemFont, 'Segoe UI',
          Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans',
          'Helvetica Neue', sans-serif;
        --font-size-sm: 0.8125rem;
        --font-size-base: 1rem;
        --font-size-md: 1.125rem;
        --font-size-lg: 1.5rem;
        --font-size-xl: 1.625rem;
        --font-size-2xl: 2.25rem;
        --font-size-3xl: 2.5rem;
        --font-size-4xl: 3rem;
        --font-size-5xl: 4rem;
        --space-0: 0;
        --space-1: 0.25rem;
        --space-2: 0.5rem;
        --space-3: 1rem;
        --space-4: 2rem;
        --space-5: 4rem;
        --color-purple: #624de3;
        --color-purple-light: #d7d1f8;
        --color-orange: #ff9066;
        --color-orange-light: #ffe8df;
        --gap: var(--space-3);
        --radius: 10px;
        --radius-full: 50px;
        --shadow: 6px 6px 54px rgba(0, 0, 0, 0.05);
        --shadow-primary: 0 6px 12px rgba(98, 77, 227, 0.3);
        --theme-color: #00234b;
        --theme-color-alt: #525f7f;
        --theme-color-primary: var(--color-purple);
        --theme-color-primary-light: var(--color-purple-light);
        --theme-color-secondary: var(--color-orange);
        --theme-color-secondary-light: var(--color-orange-light);
        --theme-color-gray: #778ca3;
        --theme-color-gray-light: #f9faff;
        --theme-background-color: #f4f7fc;
        --theme-background-color-alt: #ffffff;
        --theme-border-color: #eaedf3;
      }

      html,
      body {
        color: var(--theme-color);
        background-color: var(--theme-background-color);
        font-family: var(--font-family);
      }

      ::selection {
        background-color: #dfe2f1;
      }
    `}
  />
)

export default GlobalStyles
