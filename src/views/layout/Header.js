/** @jsx jsx */
import { jsx } from '@emotion/react'
import styled from '@emotion/styled'

import Avatar from 'components/Avatar'
import Container from 'components/Container'
import Stack from 'components/Stack'
import Text from 'components/Text'
import { useAuth } from 'contexts/auth'

const Header = () => {
  const { user } = useAuth()

  return (
    <Wrapper>
      <Nav alignment="center" distribution="between">
        <div css={{ flex: '1 1 auto' }} />
        <Stack alignment="center">
          <Avatar />
          <div>
            <Text as="p" weight="500">
              {user.fullName}
            </Text>
            <Text as="p" variant="muted" weight="500">
              {user.role}
            </Text>
          </div>
        </Stack>
      </Nav>
    </Wrapper>
  )
}

const Wrapper = styled.header`
  display: flex;
  align-items: stretch;
  background-color: var(--theme-background-color-alt);
  height: 80px;
`

const Nav = Container.withComponent(Stack)

export default Header
