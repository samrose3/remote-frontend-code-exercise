import React from 'react'
import 'focus-visible'

import Container from 'components/Container'
import Header from './Header'

import GlobalStyles from './GlobalStyles'
import './reset.css'

const Layout = ({ children }) => (
  <div>
    <GlobalStyles />
    <Header />
    <Container>{children}</Container>
  </div>
)

export default Layout
